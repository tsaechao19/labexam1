//
// Created by Tony on 9/26/2017.
//
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <random>
#ifndef LABEXAM1_MYMATRIX_H
#define LABEXAM1_MYMATRIX_H
class myMatrix{

public:
    int **matrix;
    int m;
    int n;
    myMatrix();
    myMatrix(int row,int col);
    myMatrix(std::string value, int row, int col);
    friend myMatrix operator+ (myMatrix &mat1,myMatrix &mat2);
    // friend myMatrix operator -(myMatrix &mat1, myMatrix &mat2);
    // friend myMatrix operator *(myMatrix &mat1, myMatrix &mat2);
private:


};


#endif //LABEXAM1_MYMATRIX_H
